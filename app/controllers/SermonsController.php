<?php

class SermonsController extends BaseController {

    protected $layout = 'layouts.master';
    
    public function index()
    {
        $this->layout->content = View::make('about');
    }

    public function create()
    {
        $users = User::all();
        $this->layout->content = View::make('sermons.create')->with(compact("users"));

    }

    public function store(){
        Sermon::create(Input::except('q'));
        Redirect::back();
    }

    public function edit(){
        return Sermon::orderBy('delivered_at', 'desc')->get();
    }

    public function update(){
        return Sermon::orderBy('delivered_at', 'desc')->get();
    }

}