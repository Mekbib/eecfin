<?php

namespace Controllers\Api;

class MembersController extends \BaseController {

    //protected $layout = 'layouts.master';
    
    public function index()
    {
        $sermons = \Sermon::orderBy('delivered_at', 'desc')->get();
        return $sermons;
    }
}