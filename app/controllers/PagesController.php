<?php

class PagesController extends BaseController {

    //protected $layout = 'layouts.master';
    protected $layout = 'layouts.layout';
    
    public function about()
    {
        $this->layout->content = View::make('about');
    }

    public function media()
    {

        $sermons = $this->getSermons();
        $this->layout->content = View::make('sermons')->with(compact("sermons"));

    }

    public function live(){
        $this->layout->content = View::make('live');
    }

    private function getSermons(){
        return Sermon::orderBy('delivered_at', 'desc')->get();
    }

    public function conference($title){


        $conference = Conference::with('days')->where('name', $title)->first();

        if(!$conference){
            return Redirect::to(route('eecfin.media'));
        }

        $selectedDay = $conference->days()->where('order', 1)->first();

		if($selectedDay){
			$selectedVideo = $selectedDay->videos()->where('order', 1)->first();//dd($selectedDay->videos);
		}
        

        $this->layout->content = View::make('conference')
                                        ->with(compact("conference","selectedDay","selectedVideo"));

        
    }

    public function  conferenceDay($title, $day){


        $conference = Conference::with('days')->where('name', $title)->first();

        if(!$conference){
            return Redirect::to(route('eecfin.media'));
        }

        $selectedDay = $conference->days()->where('day', $day)->first();
        $selectedVideo = $selectedDay->videos()->where('order', 1)->first();

        $this->layout->content = View::make('conference')
                                        ->with(compact("conference","selectedDay","selectedVideo"));

    }

    public function conferenceDayVideo($title, $day, $video){
        

        $conference = Conference::with('days')->where('name', $title)->first();

        if(!$conference){
            return Redirect::to(route('eecfin.media'));
        }
        $selectedDay = $conference->days()->where('day', $day)->first();
        $selectedVideo = $selectedDay->videos()->where('id', $video)->first();

//dd(  $selectedDay );//
        $this->layout->content = View::make('conference')
                                        ->with(compact("conference","selectedDay","selectedVideo"));

    }
}