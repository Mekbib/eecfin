<?php

use \Sermon;

class HomeController extends BaseController {

    protected $layout = 'layouts.home';
	

	public function showWelcome()
	{
		return View::make('hello');
	}


	public function home()
	{
		$event = "Tamrat!";
		$sermons = Sermon::orderBy('delivered_at', 'desc')->get();
        $this->layout->with(compact("event","sermons"));
	}


}