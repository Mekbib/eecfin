<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/',  
            array('as' => 'eecfin.home', 
                    'uses' => 'HomeController@home'));

Route::get('/eecfin', function()
{
    return View::make('hello');
});


Route::get('/home', 
            array('as' => 'eecfin.home', 
                    'uses' => 'HomeController@home'));

Route::get('/about', 
            array('as' => 'eecfin.about', 
                    'uses' => 'PagesController@about'));

Route::get('/media', 
            array('as' => 'eecfin.media', 
                    'uses' => 'PagesController@media'));

Route::get('/live', 
            array('as' => 'eecfin.live', 
                    'uses' => 'PagesController@live'));

Route::get('/conference/{title}', 
            array('as' => 'eecfin.conference', 
                    'uses' => 'PagesController@conference'));

Route::get('/conference/{title}/{day}', 
            array('as' => 'eecfin.conference.day', 
                    'uses' => 'PagesController@conferenceDay'));

Route::get('/conference/{title}/{day}/{video}', 
            array('as' => 'eecfin.conference.day.video', 
                    'uses' => 'PagesController@conferenceDayVideo'));

Route::get('/conference', 
            array('as' => 'eecfin.conference.index', 
                    'uses' => 'PagesController@conference'));

// Route::get('/members', 
//             array('as' => 'eecfin.members.index', 
//                     'uses' => 'MembersController@index'));

Route::get('/member', function() {   
    return File::get(public_path().'/members/index.html');
});

Route::get('/api/members', 
            array('as' => 'eecfin.api.members', 
                    'uses' => 'Controllers\Api\MembersController@index'));

Route::group(array('before' => 'auth', 'prefix' => 'super'), function(){

    Route::resource('sermons','SermonsController');
    
});



