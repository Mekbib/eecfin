
<div class="col-xs-6">


<p><h3><center> የኢትዮጵያ ወንጌላዊት ቤተክርስቲያን በፊንላንድ አጭር ታሪክ</center> </h3></p>
<br>

<p> <h5>“ጅማሬህ ታናሽ ቢሆንም እንኳ ፍጻሜህ እጅግ ይበዛል።”(ኢዮ 8፡7)</h5><br/></p>

በ 1984 ዓ.ም ከእግ ዚአብሔር በሆነ አላማ እና አሠራር ለወንድም ዳንኤል አብርሃም አጭር የትምህርት በርን በመክፈት ከኢትዮጵያ ወደ ፊንላንድ እንዲመጣ ሆነ። ይህ ኣጭር ትምህርት እንዳለቀ፣ ወቅቱ በኢትዮጵያ ለአማኝ ክርስቲያኖች የማይመች ሰለነበር በእግዚአብሔር ምሪት እና ፈቃድ ወንድም ዳንኤል በፊንላንድ የመቅረት እና ትምህርቱን የመቀጠል በር ተከፈተለት። ወንድም ዳንኤል ባገባ በ6ኛ ወሩ ሙሽራ ባለቤቱን በኢትዮጵያ ትቶ እና ከሞቀ የአማኞች ሕብረት እንዲሁም በመሰጠት ከሚያገለግልበት ቤተክርስቲያን መለየት እጅግ ከባድ ቢሆንበትም ከባለቤቱ ጋር በመመካከር እና እግዚአብሔርን በመታመን ብቻ በፊንላንድ ቀረ።  ታማኙ እግዚአብሔርም በስድስት ወራት ውስጥ በአስደናቂ አሰራሩ ለባለቤቱም ለእህት አስቴር ተፈራም
በርን በመክፈት ወደ ፊንላንድ እንድትመጣ ሆነ።

 
<h5>የወገኖች ሕብረት አጀማመር፤</h5>
 
በዚያን ወቅት ፊንላንድ ውስጥ በጣም ጥቂት ኢትዮጵያውያን በተለያየ ቦታ በትምህርት የሚገኙ መሆናቸውን የሚያውቀው ወንድም ዳንኤል ከባለቤቱ ጋር በቤታቸው ውስጥ በየጊዜው ምግብን በማዘጋጀት እነኝህን በቅርብ እና በሩቅ ያሉትን ወገኖች እየጠሩ ወንጌልን ላልሰሙ እንዲሰሙ አማኝ ለነበሩትም መበረታታት እንዲሆን ሕብረት ማድረግን ቀጠሉ። በሌላም አቅጣጫ በፊንላንድ በሔልስንኪ ባለችዉ አለም አቀፍ ቤተክርስቲያን እግዚአብሔር ለወንድም
ዳንኤል በሽምግልና እንዲያገልግል በር ሲከፍትለት በሔልሲንኪ አካባቢ የሚገኙ ኢትዮጵያውያን የሚችሉ ሁሉ በአለም አቀፉ ቤተክርስቲያን በእሁድ ጠዋቱ ፕሮግራም በመገናኘት ከዚያም አምልኮ ሲያልቅ አብሮ ተቀምጦ በቡና ዙሪያ ሕብረት በማድረግ መበረታታት እና መመካካር ቀጠለ። ከዚህም በተጨማሪ ፓስተር ሎይድ ሽዋንዝ በወቅቱ የአለም አቀፉ ቤተክርስቲያን ፓስተር የመጽሃፍ ቅዱስ ጥናት በአማርኛ በየ አስራ አምስት ቀኑ ለማድረግ ያለንን ሃሳብ ስንነግራቸው አበረታትተው በዚያን ወቅት ብዙ አንባቢ በነበረው እና በየዩኒቨርስቲዎቹ በሚላከው የቤተክርስቲያኒቱ የዜና መጽሔት ላይ የአማርኛ መጽሐፍ ቅዱስ ጥናት በየአስራ አምስት ቀኑ መጀመሩ ይፋ ሆነ። ይህም በሩቅ ያሉ በመጽሄቱ አማካኝነት አድራሻችንን በማወቅ እየደወሉልን እንድናውቃቸው፤ ሲመቻቸውና ሔልሲንኪ ሲመጡም አብረውን ሕብረት እንዲያደርጉ ረዳን።


<h5>የሰሜን አዉሮፓ የኢትዮጵያዉያን ክርስቲያኖች ሕብረት ምሥረታ፤</h5>
 
በ 1985 ዓ.ም ለወንድም ዳንኤል በስዊድን እና በኖርዌ ካሉ አማኞችም ጋር ሕብረት የማድረግ በር ስለተከፈተለት አብረው የጸሎት ሕብረት እና የአሰራር ልምድንም መለዋወጥ ተጀመረ። ይህም ግንኙነት ለዛሬዉ መጠነ ሰፊ ሕብረታችን መነሻና መሠረት መሆኑን ያመለክታል።


<h5>የመጀመሪያው የሔልሲንኪ ኮንፍራንስ፤</h5>
 
በ1987 ዓ.ም ወንድም ወልደ አምላክ ዲኬ በሥራ በአጋጣሚ መምጣታቸው እና ዘማሪ 50 አለቃ ሥዩም ገብረየሱስ የሉተራን ቤተክርስቲያን ጠርታቸው በመምጣታቸው ምክንያት የመጀመሪያውን የፊንላንድ ኮንፍራንስ በፊኒሽ መጽሐፍ ቀዱስ ት/ቤት በሔልሲንኪ መካከል በነበራቸው መሰብሰቢያ ቦታ እንድናካሂድ
ሆነ። በዚህ ኮንፍራንስ በአጠቃላይ ከመላው ፊንላንድ የተመቻቸው ሁሉ መጥተው ፕሮግራሙን ተካፈሉ። ይህም ጊዜ እግዚአብሔር በወንድም ወልደ አምላክ ዲኬ ምስክርነት እንዲሁም በ 50 አለቃ ስዩም ገብረየሱስ ዝማሬ የተባረክንበት፤ እንዲሁም በፊንላንድ ለመጀመሪያ ጊዜ እህቶች እንጀራ በመጋገር ምግብ የበላንበት እና በመንፈስም በስጋም የታደስንበት፤ ለማያምኑም ወንጌል የተሰበከበት ወቅት ነበር። በዚህ ስብስባ ወቅት መጽሃፍ ቅዱስ ጥናት እንዲጠናከር እና አገልጋይም በመጣ ወቅት ተጠራርተን ለመገናኘት ወስነን መለያየት ግድ ስለሆነ ብቻ ተለያየን። ሆኖም ከዚያ ጊዜ ጀምሮ ጠንከር ባለ ሁኔታ የመጽሐፍ ቅዱስ ጥናት ቢቀጥልም ከመራራቅ እና ካለመመቻቸት የተነሳ አንዳንዴ ስንበዛ አንዳንዴ ስናንስ ህብረቱ ቀጠለ። ከዚያም ወንድም መለሰ ወጉን ደግሞ እግዚአብሔር በዚያው ዓመት ልኮልን ጥቂቶችን አገልግለውን ነበር። በተለይ እርሳቸው አለም አቀፍ አገልግሎት በመጀመራቸው ወርሃዊ ካሴቶችን ትምህርት እና ዝማሬ ይልኩልን ስለነበር እነኝህን ካሴቶቻቸውን እየተዋዋስን እንዲሁም ከመጽሃፍ ቅዱስ ጥናት በኋላ ከወገኖች ጋር አብረን እናዳምጣቸው
ነበር።
 

<h5>የወንድም አቤል ወደ ፊንላንድ አመጣጥ፤</h5>
 
እንዲህ እያልን አንዴ ስንበረታ አንዴ ስንደክም ጌታንም እባክህ የዚህን አገር ስራ ኣጠንክረው፣ አስፋው አብሮንም የሚቆም ሰው ስጠን እያልን ስንጸልይ ወንድም አቤል ገብሬ እግዚአብሔር በሚያስደንቅ ሁኔታ በርን ከፍቶለት በ 1989 ዓ.ም መጣ። ከወንድም አቤል ጋር ወዲያው ተገናኘን እና የሚያስደንቅ
ምስክርነቱንነገረን። እንዴት ጌታን እንዳገኘ፤ እንዴት ማገልገል እንደ ጀመረ ነገረን። እኛም እየተደነቅን እግዚአብሔርን አመሰገን። ወንድም አቤል እግዚአብሔር አብሮን እንዲያገልግል በትክክል መመሪያ ሰጥቶትእንደመጣ ሲነግረን ጸሎትን የሚመልሰውን ታማኙን ጌታችንን አመስግነን በየአስራ አምስት ቀኑ የነበረው መጽሐፍ ቅዱስ ጥናት በየሳምንቱ እንዲሆን በቤት መሆኑም ልጆችን ለመያዝ ስለሚያመች በወንድም ዳንኤል ቤት እንዲሆን ሆኖ በተጠናከረ ሁኔታ ቀጠለ። ወንድም አቤል ከዚያ በቤት ውስጥ ከሚሰበስቡት እህቶች ጋርም መዝሙር መለማመድ ቀጥለው የመዘምራን ቡድን ተመሰረተ።


<h5>የሔልሲንኪ የኢትዮጵያ ወንጌላውያን ክርስቲያኖች ሕብረት ምሥረታና የወንጌላዊ ሹመት፤</h5>

ቁጥራችንም እየበዛ ሲመጣ እንደገና ሔልሲንኪ ከተማ ወዳለው ፊኒሽ መጽሃፍ ቅዱስ ትምህርት ቤት ማእከል የስብሰባ ቦታ በመመለስ ጠንከር ባለ ሁኔታ የኢትዮጵያ ወንጌላውያን ክርስቲያኖች ሕብረት ተብለን ብዙዎችን በፍቅር የሳበ አገልግሎት ጀመርን:: ይህም ህብረት በሰሜን አውሮፓ አብያተክርስቲያናት ሕብረትም ትጉ ተሳትፎ ያደርግ ነበር። በ 1990 ዎቹ መጀመሪያ ብዙ ኢትዮጵያውያን ወደ ፊንላንድ በመምጣታቸው የተነሳ እነሱ ወደ ሚኖሩበት አካባቢ እየሄድንም በማገልገል ጥቂቶችን ወደ ጌታ ቤት እንዲመጡ እንጥር ነበር። ወንድም አቤልም እግዚአብሔር በሰጠው ራእይ መሰረት እግዚአብሄር የተናገረውን በመንገር፣ በጸሎት በመትጋት፣ በማማከር እና በዝማሬ አገልግሎት የተጋ ወንድም ስለነበረ በሔልስንኪ የኢትዮጵያ ወንጌላውያን ክርስቲያኖች ሕብረት የሙሉ ጊዜ አገልጋይ አድርገው በወንጌላዊነት እንዲሾሙልን ፓስተር ሃይሌ ወ/ሚካኤልን ጠይቀን እሳቸውም በፈቃደኝነት መጥተዉ በህዝብ ፊት ቃል እንዲገባ አድርገው አገልግሎቱን በወንጌላዊነት በይፋ ጀመረ። ይህም ከሆነ ጀምሮ በሕብረታችን ትልቅ የመንፈስ ቅዱስ ማበረታታት እና ማጽናናት እየሆነ ቁጥራችንም
ማደግ ጀመረ። ከዚያም ይህ ህብረት በጸሎት የእምነት እርምጃን በመውሰድ አጠቃላይ ጉባዔ ተጠርቶ መተዳደሪያ ደንብ ከጸደቀ በኋላ በእግዚአብሔእር ፈቃድ ሕብረቱ በ 1993 ወደ ቤተክርስቲያንነት ተለወጠ። የቤተክርስቲያኒቱም ስም “የኢትዮጵያ ወንጌላዊት ቤተክርስቲያን በሔልሲንኪ” ሆኖ ይፋ አገልግሎት
መስጠት ተጀመረ። በዚህም ወቅት ድሮ ከምናመልክበት ከ ፊኒሽ መጽሐፍ ቅዱስ ትምህርት ቤት ማዕከል ለቀን በቤቴል ባፕቲስት ቤተክርስቲያን የአምልኮ ማዕከላችን አደረግን። በዚህ ቦታ ለበርካታ ዓመታት ማለትም እስከ 2002 መጨረሻ ድረስ ከቆየን በኋላ ከ2003 ጀምሮ ደግሞ በአለም አቀፍ ቤተክርስቲያን የሥራ ማእከል በመዛወር እስከ 2010 መጨረሻ ድረስ በዚያ ቦታ እያመልከን ቆየን።
<br/><br/><h5>የፓስተር አቤል ሹመት፤</h5><br/>

ወንጌላዊ አቤል በወንጌላዊነት እያገለገለ ሳለ ከምዕመናን መብዛት እና ከአገልግሎቱም መስፋት የተነሳ ቤተክርስቲያናችን በመጋቢ የመመራት አስፈላጊነት ስለታመነበት እረኛ እንዲሰጠን በእግዚአብሔር ፊት በጸሎት ቆየን። ወንጌላዊ አቤልም ”የወንጌል ሰባኪነትን ሥራ አድርግ አገልግሎትህንም ፈጽም” ለሚለው
የእግዚአብሔር ቃል የታዘዘና እንዲሁም ማንን እልካለሁ ማንስ ይሄድልናል ለሚለው እኔን ላከኝ ብሎ መልስ የስጠ ፣ አገልግሎቱንም በትጋት ያካሄደ መሆኑን በመረዳት የቤተክርስቲያናችን ሽማግሌዎችና ምዕመናን ወንጌላዊ አቤል ፓስተር እንዲሆን በሙሉ ድምፅ አፀደቁ። በዚህም መሠረት ፓስተር ዳንኤል መኰንን የሹመቱን ሥነሥርዓት እንዲያከናዉኑልን ተጠይቀዉ የቤተክርስቲያኒቱንም ምሥክርነት ካረጋገጡ በኋላ መልካም ፈቃዳቸዉ ሆኖ በፊንላንድ ሔልሲንኪ ተገኝተው በ 20.08. 2005 ፓስተር አድርገው ሾሙት። ፓስተር አቤል ገብሬም ቤተክርስቲያናችንን በቅንነትና፣ በታማኝነት ሲያገለግል በድንገተኛ ሕመም በሆስፒታል ሲረዳ ከቆየ በኋላ ሴፕቴምበር 20.ቀን 2008 ወደ ጌታ ሄዷል። ፓስተር አቤል በፍቅሩ፤ በታማኝነቱ፤ በእግዚአብሔር ቃል መሠረት በብዙ ነገሮች ምሳሌ ሆኖናል። ጥሪውንም ከምንም እና ከማንም በላይ በማስቀደሙ፤ በጸሎት ህይወቱ፤ በትምህርቶቹ፤ በስብከቶቹ እና በዝማሬዎቹ
ሁል ጊዜ እናስታውሰዋለን። በተለይም ሌት ተቀን በብርድ በሙቀት ሳይል ይህች በሔልሲንኪ ያለች ቤተከርስቲያን የጌታን ብርሃን እንድታበራና መልካም ምሳሌ እንድትሆን በግል ለፈለገው ወይንም በህብረት የጌታን ቃል በመመገብ፤ በችግር ያሉትን በማጽናናት፤ ለታመሙት አብሮ በመጸለይ፤ ከትንሽ እስከ ትልቅ እኩል በማየት የፈጸመውን አገልግሎት ይህች ቤተክርስቲያን በታሪኳ ሁሉ የማትረሳው ነው፡፤ እስከ አሁንም በቤቱ አገልጋዮችን ያላሳጣን እግዚአብሔር ይመስገን።

አሜን።
<br/><br/>
</font>
</p>




</div>



<div class="col-xs-6">
<h3> <center>Statement of Faith </center></h3>

<p><br><br>
   1.  We believe all 66 books making up the Old and New Testaments are infallible and inspired by God. 
   They are of supreme and final authority in matters of faith and life (II Tim. 3:16; II Peter 1:19-2; 
   Isaiah 40:8; Matthew 24:35).<br>
<br><br>
2. We believe in one God, eternally existing in three persons: Father, Son and Holy Spirit 
(I Timothy 2:5; I Corinthians 8:6; Exodus 20:2-3; Deuteronomy 6:4; John 14:16, 18,23,26: I John 5:7).<br>
<br><br>
   3. We believe that Jesus Christ was begotten by the Holy Spirit, was born of the Virgin Mary, 
   and is both true God and true man (Matthew 1:18, 23-25; Luke 1:35; I Timothy 2:5; Philippians 2:6-8).<br>
<br><br>
   4. We believe that the Lord Jesus Christ died for our sins, according to the Scriptures,
    as a representative and substitutionary sacrifice; and that all who believe in Him are 
    justified on the basis of His shed blood (Romans 3:24-26; Galatians 2:16; Romans 5:1,9;
     I Peter 2:24; I Corinthians 15:3).<br>
<br><br>
   5. We believe in the resurrection of the crucified body of our Lord Jesus Christ, 
   in His ascension into Heaven, and in His present life there for us, as our 
   High Priest and Advocate (John 11:25; Philippians 3:10; Matthew 28:6-7; Acts 1:9; 
   I Corinthians 15:4; Hebrews 4:14-15; I John 2:1).<br>
<br><br>
   6. We believe that man was created in the image of God; that he sinned and thereby incurred,
    not only physical death, but also that spiritual death which is separation from God; 
    and that all human beings are born with a sinful nature, and sin in thought, 
    word and deed (Genesis 1:26-27; Romans 3:23; I John 1:8, 10; Psalms 51:5).<br>
<br><br>
   7. We believe that all who receive by faith the Lord Jesus Christ are born again of 
   the Holy Spirit and thereby become children of God (John 3:3-5; II Corinthians 5:17; 
   Ephesians 2:1-10; II Peter 1:4; Titus 3:5-7; John 1:12-13).<br>
<br><br>
   8. We believe in the Church in its universal aspect comprising the whole body of those
    who have been born of the Spirit; and its local expression established for mutual 
    edification and witness (Ephesians 1:22-23; 5:25-32; Acts 15:41; 16:5).<br>
<br><br>
   9. We believe in the baptism of believers by immersion and the celebration of the 
   Lord’s supper (Acts 22:16; Matthew 28:19-20; I Corinthians 11:23-26; Acts 8:38: Matthew 3:16).<br>
<br><br>
  10. We believe in the Holy Spirit, the Baptism of the Holy Spirit and the gifts of 
  the Holy Spirit (Joel 2:28-32; Acts 2; Ephesians 5:18; 1Corinthians 12-14).<br>
<br><br>
  11. We believe in the Spirit-filled life for victorious Christian living and 
  effective service, and in worldwide evangelization through missionary activity 
  (Ephesians 5:18; Acts 6:3; Matthew 28:19; Acts 1:8; John 10:27-29; Ephesians 1:13-14).<br>
<br><br>
  12. We believe in the presence of Satan, who is also called the devil. 
  He, with his demons, is in continual warfare with the workings of God. 
  His activities among men began at the fall and will continue until his 
  final doom (Matthew 4:1-11; 25:31; Revelation 20:2, 10; II Corinthians 11:3f.).<br>
<br><br>
  13. We believe in the bodily resurrection of the just and the unjust, 
  the eternal blessedness of the saved and the everlasting punishment of 
  the lost (I Corinthians 15:21-23; II Thessalonians 1:7-10; Revelation 20:11-15; Matthew 25:34).<br>
<br><br>
  14. We believe in the imminent return of our Lord and Savior Jesus Christ 
  (John 14:3; Acts 1:19-21; I Thessalonians 4:16-17; James 5:8; Hebrews 9:28; Titus 2:13)</p>
</div>



