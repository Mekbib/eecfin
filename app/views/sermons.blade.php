

    
    <div class="row">
      <div class="col-xs-12">
            <h2>Teachings and Sermons</h2>
                <div class="search-form">
                  <h4>
                     እምነት ከመስማት ነው መስማትም በእግዚአብሔር ቃል ነው። ሮሜ 10:17
                  </h4>
                  <h4>
                    faith cometh by hearing, and hearing by the word of God.
                    Romans 10:17(KJV)                  
                  </h4>
                  
                </div>
      </div>
  </div>


    <?php $i = 0; ?>
    @foreach( $sermons as  $sermon )
        @if($i%3 == 0)
            <div class="row">
                <div class="col-xs-12">
        @endif
                <div class="col-xs-12 col-sm-4">
                    <div class="news-item bordered nav-wrap" >
                        <h4 class="news-item-title text-center">  {{ $sermon->title }} </h4>
                        <h5 class="sermon-content text-center">
                            @if($sermon->subtitle) {{ $sermon->subtitle }} @else &nbsp; @endif
                        </h5>


                      <p class="text-center">
                        @if($sermon->sermon_type == "AUDIO") 
                          <a href="{{URL::to('/sermons/'.$sermon->audio)}}" target="_blank"> 
                              <img src="/img/{{ $sermon->image }}">
                          </a>
                        @elseif($sermon->sermon_type == "CONFERENCE")   
                          <a href="{{URL::to('/conference/'.$sermon->title)}}" > 
                              <img src="/img/{{ $sermon->image }}">
                          </a>
                        @endif
                      </p>
                      <h4 class="sermon-content text-center"> 
                        {{ $sermon->user->title}} 
                        {{ $sermon->user->first_name }} {{ $sermon->user->last_name }} 
                      </h4>
                        <p class="sermon-content text-center">
                           {{ date('d.m.Y', strtotime($sermon->delivered_at)) }}
                        </p>
                    </div>
                </div>

        
        <?php $i++; ?>

        @if($i%3 == 0)
                </div>
            </div>
            <br>
        @endif


        
    @endforeach


