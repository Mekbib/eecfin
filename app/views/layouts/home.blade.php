<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta charset="utf-8">
    <meta name="author" content="eecfin">
    <meta name="keywords" content="Ethiopian evangelical christian church fellowship in Helsinki Finland">
    <meta name="language" content="en">


    <meta name="description" content="Ethiopian evangelical christian church in Helsinki Finland.
    Ethiopian and Eritrean Christian fellowship in Helsinki Finland">

    <title>Ethiopian Evangelical Church in Helsinki Finland</title>




    <link rel="icon" href="/favicon.ico">
    <!--[if IE]><link rel="shortcut icon" href="/favicon.ico"><![endif]-->

    <!-- Bootstrap core CSS -->
    <link href='http://fonts.googleapis.com/css?family=Lato:400,700' 
    rel='stylesheet' type='text/css'>
    <link href="css/styles.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/front.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="/assets/js/html5shiv.js"></script>
<script src="/assets/js/respond.min.js"></script>
<![endif]-->

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-8563518-3', 'eecfin.org');
    ga('send', 'pageview');

</script>
</head>

<body>


    

    <div class="container-fluid">
       
            <div class="col-xs-12 logo text-center">
                
                <p>የኢትዮጲያ ወንጌላዊት ቤተክርስቲያን በፊንላንድ</p>
                <p>Ethiopian  Evangelical Church Finland</p>
            </div>

        

        <div class="row">
            <div class="col-xs-12 menu-row">
                <div class="hidden-xs col-sm-1" style="background-color: #474D50; height: 90px"></div>
                <a href="/">
                    <div class="hidden-xs col-sm-2 text-center menu-item @if(Request::is('/')) active @endif">
                        HOME
                    </div>
                </a>
                <a href="/media">
                    <div class="col-xs-4 col-sm-3 text-center menu-item @if(Request::is('/media')) active @endif">
                        SERMONS
                    </div>
                </a>
                <a href="/live">
                    <div class="col-xs-4 col-sm-2 text-center menu-item @if(Request::is('/live')) active @endif">
                        LIVE
                    </div>
                </a>
                <a href="#contact">
                    <div class="col-xs-4 col-sm-3 text-center menu-item">
                        CONTACT
                    </div>
                </a>
                <div class="hidden-xs col-sm-1" style="background-color: #474D50; height: 90px"></div>
            </div>
        </div>
        

        <!--<div class="col-xs-12 line-divider"></div>-->


        <div class="row">
            <div class="col-xs-12 second-row">
                <div class="col-xs-12 col-sm-8 weekly">
                    <h2>WEEKLY</h2></th>

                    <div class="col-xs-12 col-sm-11">
                    <table class="table table-" cellpadding="20">
                        <tr>
                            <td>TUESDAYS</td>
                            <td>(18:00 - 19:00)</td>
                            <td>BIBLE STUDY</td>
                        </tr>
                        <tr>
                            <td>THURSDAYS</td>
                            <td>(18:00 - 20:00)</td>
                            <td>PRAYER</td>
                        </tr>
                        <tr>
                            <td>SUNDAYS</td>
                            <td>(14:00 - 16:00)</td>
                            <td>WORSHIP</td>
                        </tr>
                        <tr>
                            <td colspan="3" class="warm-welcome"><h4 >Warm Welcome!</h4></td>
                        </tr>
                    </table>
                    </div>
                    
                    

                </div>
                <div class="col-xs-12 col-sm-4 who-is-jesus">
                    <h2>WHO IS JESUS<br> THAT<br> I<br> MAY BELIEVE<br> IN HIM<h2>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-xs-12 third-row">
                
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <a href="/about">
                        <h1><i class="fa fa-users"></i><br><br>ABOUT</h1>
                        <h5><strong>Church history | Faith statment</strong></h5>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <h1><i class="fa fa-university"></i><br><br>RECOURCES</h1>
                    <h5><i>(coming soon)</i></h5>
                </div>
                
                <div class="col-xs-12 col-sm-12 col-md-4 recent-sermon">
                    @if($sermons)
                        <?php $sermon = $sermons[0]; ?>

                            <h1><i class="fa fa-microphone"></i><br><br>RECENT SERMON</h1>
                            <a href="{{URL::to('/sermons/'.$sermon->audio)}}" target="_blank"> 
                              <h4 class="news-item-title">  {{ $sermon->title }} </h4>
                              @if($sermon->subtitle)
                                    <h5> {{ $sermon->subtitle }} </h5>
                              @endif 
                            </a>
                              <h5> 
                                {{ $sermon->user->title}} 
                                {{ $sermon->user->first_name }} {{ $sermon->user->last_name }} 
                              </h5>  
        
                    @endif


                    
                </div>
<!--
                <div class="col-xs-12 col-sm-4">
                    <h1><i class="fa fa-heartbeat"></i><br><br>YOUTH</h1>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <h1><i class="fa fa-ship"></i><br><br>UPCOMING</h1>
                </div>
                <div class="col-xs-12 col-sm-4"><h1>RECENT 2</h1></div>
                -->
            </div>
        </div>
        
        <div class="row">
            <div class="col-xs-12 fourth-row" id="contact">
                 <div class="col-xs-12 col-sm-4">

                        <h3><p>CONTACTS</p></h3>

                                <strong>Pastor Muluwork Mekuria</strong><br>
                                email: mifirstm@gmail.com<br>
                                tel: +358-449-992-221 <br>
                                  <br>
                                <strong>Evangelist Zegeye Mekonnin</strong><br>
                                email: zege06@yahoo.com<br>
                                tel: +358-444-121-119<br><br>

                                <strong>Yeteshawork Bogale</strong><br>
                                email: yeteshaworkb@gmail.com<br>
                                tel: +358-415-225-889<br><br>
                                <strong>WM:</strong> Mekbib Tekle: mekbib.awono@gmail.com
                 </div>

                 <div class="col-xs-12 col-sm-4">
                    <h3><p>&nbsp;</p></h3>
                                <strong>Keinulaudankuja 4 B,</strong><br>
                                00940 Helsinki<br>
                                Finland<br><br>
                                <strong>Bank account:</strong><br>
                                Nordea: IBAN FI60122030 00177960<br><br>

                                <strong>Building Fund Account No.</strong><br>
                                 IBAN FI4815553000115783<br><br><br>

                 </div>
                     
                               
                 <div class="col-xs-12 col-sm-4" style="padding-top: 50px">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1980.6634041700947!2d25.080751916109108!3d60.23594438198516!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x469208b703070d57%3A0xe8e0df43f9f05652!2sKeinulaudankuja+4%2C+00940+Helsinki%2C+Finland!5e0!3m2!1sen!2sus!4v1460235863063" width="300" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
                 </div>
            </div>
        </div>


    </div>

    <script>
        $('.logo').on('click', function(){
            window.location.href = "/";
        });
    </script>

    
<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="//code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script src="js/i18n/jquery.ui.datepicker-fi.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="assets/js/respond.min.js"></script>
    <script src="js/app.js"></script>
</body>
</html>
