<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="author" content="eecfin">
    <meta name="keywords" content="Ethiopian evangelical christian church fellowship in Helsinki Finland">
    <meta name="language" content="en">


    <meta name="description" content="Ethiopian evangelical christian church in Helsinki Finland.
    Ethiopian and Eritrean Christian fellowship in Helsinki Finland">

    <title>Ethiopian Evangelical Church in Helsinki Finland</title>




    <link rel="icon" href="/favicon.ico">
    <!--[if IE]><link rel="shortcut icon" href="/favicon.ico"><![endif]-->

    <!-- Bootstrap core CSS -->
    <link href='http://fonts.googleapis.com/css?family=Lato:400,700' 
    rel='stylesheet' type='text/css'>
    <link href="css/styles.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/front.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="/assets/js/html5shiv.js"></script>
<script src="/assets/js/respond.min.js"></script>
<![endif]-->

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-8563518-3', 'eecfin.org');
    ga('send', 'pageview');

</script>
</head>

<body>


    

    <div class="container-fluid">
       
        <div class="col-xs-12 logo text-center">
            
            <p>የኢትዮጲያ ወንጌላዊት ቤተክርስቲያን በፊንላንድ</p>
            <p>Ethiopian  Evangelical Church Finland</p>
        </div>

        

       
        

        <div class="row">
            <div class="col-xs-12 menu-row">
                <div class="hidden-xs col-sm-1" style="background-color: #474D50; height: 90px"></div>
                <a href="/">
                    <div class="hidden-xs col-sm-2 text-center menu-item @if(Request::is('/')) active @endif">
                        HOME
                    </div>
                </a>
                <a href="/media">
                    <div class="col-xs-4 col-sm-3 text-center menu-item @if(Request::is('media')) active @endif">
                        SERMONS
                    </div>
                </a>
                <a href="/live">
                    <div class="col-xs-4 col-sm-2 text-center menu-item @if(Request::is('live')) active @endif">
                        LIVE
                    </div>
                </a>
                <a href="#contact">
                    <div class="col-xs-4 col-sm-3 text-center menu-item">
                        CONTACT
                    </div>
                </a>
                <div class="hidden-xs col-sm-1" style="background-color: #474D50; height: 90px"></div>
            </div>
        </div>
        

            {{ $content }}
          
    </div>
    @include("partials.footer")
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

    <script>
        $('.logo').on('click', function(){
            window.location.href = "/";
        });
    </script>

  </body>
</html>
