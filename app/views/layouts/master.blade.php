<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="author" content="eecfin">
    <meta name="keywords" content="Ethiopian evangelical christian church fellowship in Helsinki Finland">
    <meta name="language" content="en">


    <meta name="description" content="Ethiopian evangelical christian church in Helsinki Finland.
    Ethiopian and Eritrean Christian fellowship in Helsinki Finland">

    <title>Ethiopian Evangelical Church in Helsinki Finland</title>
          



    <link rel="icon" href="/favicon.ico">
    <!--[if IE]><link rel="shortcut icon" href="/favicon.ico"><![endif]-->

    <!-- Bootstrap core CSS -->
    <link href='http://fonts.googleapis.com/css?family=Lato:400,700' 
        rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ URL::asset('css/styles.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/front.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="/assets/js/html5shiv.js"></script>
      <script src="/assets/js/respond.min.js"></script>
    <![endif]-->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-8563518-3', 'eecfin.org');
      ga('send', 'pageview');

    </script>

  </head>

  <body>
   
    <div class="top-line"></div>
    @include("partials.header")

    <div class="container">

      <div class="row">
          @include("partials.menu") 
          
          <div class="col-xs-12 content">
            {{ $content }}
          </div>
        </div>
      </div>
    </div>
    @include("partials.footer")
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

  </body>
</html>
