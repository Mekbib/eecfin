
<div class="row">
  <div class="col-xs-12">
    <div class="">
      

        <div class="row  text-center">
            <h4>{{ $conference->display_name }}</h4><br>
        </div>

        <div class="col-xs-3 col-sm-3 col-md-3" style="background-color: #d9edf7; min-height: 310px;">
            <ul class="nav nav-pills nav-stacked">

                <li class="col-md-12">
                    
                    <hr>
                </li>
            @foreach($conference->days as $day)
                <li class="col-md-12">
                    <a  class="@if(strtolower($selectedDay->day)== strtolower($day->day)) selected @endif" href="/conference/{{ $conference->name}}/{{$day->day}}">{{ $day->day }}</a>
                    <hr>
                </li>
            @endforeach
            </ul>
        </div>

        <div class="col-xs-9 col-sm-9 col-md-9">
            
            <div class="row">
                <div class="col-sm-2">
                    
                </div>
                <div class="center-block">
                    <video width="480" height="320" controls>
                        <source src="/sermons/{{$selectedVideo->filename}}" type="video/mp4">
                        Your browserdoes not support the video tag.
                    </video>
                </div>
            </div>
            <div class="row" style="background-color: #d9edf7" >
                @foreach($selectedDay->videos as $video)
                    <div class="col-sm-2" style="padding: 20px;">
                        <a href="/conference/{{ $conference->name}}/{{$selectedDay->day}}/{{ $video->id }}"
                           >
                            <img src="{{ "/img/".$video->image }}" width="120px" 
                            class="pull-left  @if($selectedVideo->id == $video->id) selected @endif"  />  
                        </a>
                        <span class="text-center">{{ $video->display_name }}  </span>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
  </div>  
</div>