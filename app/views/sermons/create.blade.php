
{{ Form::open(array('route' => 'super.sermons.store', 'method' => 'post', 'class' => 'form-horizontal')) }}
    <div class="inner">
        <fieldset>
            <legend>Add sermon</legend>
            <div class="control-group">
                <label class="control-label" for="code">Preacher</label>
                <div class="controls">
                    <select name="user_id" id="">
                        @foreach($users as $user)
                            <option value="{{ $user->id }}"> {{ $user->first_name }} {{ $user->last_name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="title">Title</label>
                <div class="controls">
                    <input class="input-large" name="title" id="name" type="text" value="{{ Input::old('title') }}">
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="subtitle">Sub title </label>
                <div class="controls">
                    <input class="input-large" name="subtitle" id="subtitle" type="text" value="{{ Input::old('subtitle') }}">
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="image">Image file</label>
                <div class="controls">
                    <input class="input-large" name="image" id="image" type="text" value="{{ Input::old('image') }}" placeholder="eg. cross.jpg">
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="audio">Audio file</label>
                <div class="controls">
                    <input class="input-large" name="audio" id="audio" type="text" value="{{ Input::old('audio') }}" placeholder="eg. 26_01_14_muluwork.mp3">
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="delivered_at">Delivered at</label>
                <div class="controls">
                    <input class="input-large" name="delivered_at" id="delivered_at" type="text" value="{{ Input::old('delivered_at') }}" placeholder="eg. 2014-01-26 13:15:38">
                </div>
            </div>
        </fieldset>
        
    </div>
    <div class="form-actions">
        <a href="{{ route('super.sermons.index') }}" class="btn btn-warning  btn-large">Takaisin</a> 
        <button type="submit" class="btn btn-success btn-large">Tallenna</button>
    </div>
{{ Form::close() }}
