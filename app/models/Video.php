<?php

use LaravelBook\Ardent\Ardent;

class Video extends Ardent {

    protected $table = 'videos';

    protected $guarded = array();

    public static $rules = array(
    );

    public function days()
    {
        return $this->belongsTo('ConferenceDay');
    }

}