<?php

use LaravelBook\Ardent\Ardent;

class ConferenceDay extends Ardent {

    protected $table = 'conference_days';

    protected $guarded = array();

    public static $rules = array(
    );

    public function conference()
    {
        return $this->belongsTo('Conference');
    }

    public function videos(){

        return $this->hasMany('Video');
    }

}