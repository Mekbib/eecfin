<?php

use LaravelBook\Ardent\Ardent;

class Conference extends Ardent {

    protected $table = 'conference';

    protected $guarded = array();

    public static $rules = array(
    );

    public function days()
    {
        return $this->hasMany('ConferenceDay');
    }

}