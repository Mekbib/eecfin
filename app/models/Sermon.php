<?php

use LaravelBook\Ardent\Ardent;

class Sermon extends Ardent {

    protected $table = 'sermons';

    protected $guarded = array();

    public static $rules = array(
    );

    public function user()
    {
        return $this->belongsTo('User');
    }

}