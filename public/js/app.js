

var popupClosed = sessionStorage.getItem('popupClosed');

if (popupClosed !== "true"){
	$('#constitution-popup').show();	
	$('#constitution-bg').show();	
}

$("#constitution-close").on('click', function(){
	$('#constitution-popup').hide();	
	$('#constitution-bg').hide();	
	sessionStorage.setItem('popupClosed', 'true');

	sessionStorage.setItem('pageLoadCounter', 1);
});


var pageLoadCounter = sessionStorage.getItem('pageLoadCounter');
if (pageLoadCounter){
	pageLoadCounter = parseInt(pageLoadCounter,10);
	sessionStorage.setItem('pageLoadCounter', ++pageLoadCounter);
		
	if (pageLoadCounter > 3){
		sessionStorage.setItem('popupClosed', 'false');
		sessionStorage.setItem('pageLoadCounter', 1);
	}
}
