System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var Member;
    return {
        setters:[],
        execute: function() {
            Member = (function () {
                function Member(id, firstName, lastName, elderId) {
                    this.id = id;
                    this.firstName = firstName;
                    this.lastName = lastName;
                    this.elderId = elderId;
                }
                return Member;
            }());
            exports_1("Member", Member);
        }
    }
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIk1lbWJlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7O1lBQUE7Z0JBQ0MsZ0JBQW1CLEVBQVUsRUFBUyxTQUFpQixFQUFTLFFBQWdCLEVBQVMsT0FBZTtvQkFBckYsT0FBRSxHQUFGLEVBQUUsQ0FBUTtvQkFBUyxjQUFTLEdBQVQsU0FBUyxDQUFRO29CQUFTLGFBQVEsR0FBUixRQUFRLENBQVE7b0JBQVMsWUFBTyxHQUFQLE9BQU8sQ0FBUTtnQkFFeEcsQ0FBQztnQkFDRixhQUFDO1lBQUQsQ0FKQSxBQUlDLElBQUE7WUFKRCwyQkFJQyxDQUFBIiwiZmlsZSI6Ik1lbWJlci5qcyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBjbGFzcyBNZW1iZXJ7XG5cdGNvbnN0cnVjdG9yKHB1YmxpYyBpZDogbnVtYmVyLCBwdWJsaWMgZmlyc3ROYW1lOiBzdHJpbmcsIHB1YmxpYyBsYXN0TmFtZTogc3RyaW5nLCBwdWJsaWMgZWxkZXJJZDogbnVtYmVyKSB7XG5cblx0fVxufSJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
