System.register(['angular2/core', './members.service', './elders.service', './Member', 'angular2/router'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, members_service_1, elders_service_1, Member_1, router_1;
    var MembersComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (members_service_1_1) {
                members_service_1 = members_service_1_1;
            },
            function (elders_service_1_1) {
                elders_service_1 = elders_service_1_1;
            },
            function (Member_1_1) {
                Member_1 = Member_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            }],
        execute: function() {
            MembersComponent = (function () {
                function MembersComponent(membersService, eldersService, router, injector) {
                    this.membersService = membersService;
                    this.eldersService = eldersService;
                    this.router = router;
                    this.params = injector.get(router_1.RouteParams);
                    this.selectedMember = new Member_1.Member(0, "", "", 0);
                }
                MembersComponent.prototype.getMembers = function (elderId) {
                    console.log('getMembers: ' + elderId);
                    //this.membersService.getMembers(elderId).then(members => this.members = members);
                    this.members = this.membersService.getMembers(elderId);
                    this.elderName = this.eldersService.getElder(elderId).firstName;
                };
                MembersComponent.prototype.getAll = function () {
                    var _this = this;
                    this.membersService.getAll().then(function (members) { return _this.members = members; });
                };
                MembersComponent.prototype.goHome = function () {
                    this.router.navigate(['/Elders']);
                };
                MembersComponent.prototype.showDetail = function (member) {
                    if (this.selectedMember.id === member.id) {
                        this.selectedMember.id = 0;
                        return;
                    }
                    this.selectedMember = this.membersService.getMember(member);
                };
                MembersComponent.prototype.ngOnInit = function () {
                    this.elderId = this.params.get('elderId');
                    console.log(this.elderId + " : elderId");
                    if (this.elderId) {
                        this.getMembers(this.elderId);
                    }
                    else {
                        this.getAll();
                    }
                };
                __decorate([
                    core_1.Input(), 
                    __metadata('design:type', Array)
                ], MembersComponent.prototype, "members", void 0);
                MembersComponent = __decorate([
                    core_1.Component({
                        selector: 'members',
                        templateUrl: 'templates/members.tpl.html',
                        providers: [members_service_1.MembersService, elders_service_1.EldersService]
                    }), 
                    __metadata('design:paramtypes', [members_service_1.MembersService, elders_service_1.EldersService, router_1.Router, core_1.Injector])
                ], MembersComponent);
                return MembersComponent;
            }());
            exports_1("MembersComponent", MembersComponent);
        }
    }
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1lbWJlcnMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1lBYUE7Z0JBUUMsMEJBQW9CLGNBQThCLEVBQ3ZDLGFBQTRCLEVBQzVCLE1BQWMsRUFDdEIsUUFBa0I7b0JBSEQsbUJBQWMsR0FBZCxjQUFjLENBQWdCO29CQUN2QyxrQkFBYSxHQUFiLGFBQWEsQ0FBZTtvQkFDNUIsV0FBTSxHQUFOLE1BQU0sQ0FBUTtvQkFFeEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUMsR0FBRyxDQUFDLG9CQUFXLENBQUMsQ0FBQztvQkFDeEMsSUFBSSxDQUFDLGNBQWMsR0FBRSxJQUFJLGVBQU0sQ0FBQyxDQUFDLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDL0MsQ0FBQztnQkFFRCxxQ0FBVSxHQUFWLFVBQVcsT0FBTztvQkFDakIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLEdBQUcsT0FBTyxDQUFDLENBQUM7b0JBQ3RDLGtGQUFrRjtvQkFDbEYsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQztvQkFDdkQsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxTQUFTLENBQUM7Z0JBQ2pFLENBQUM7Z0JBRUQsaUNBQU0sR0FBTjtvQkFBQSxpQkFFQztvQkFEQSxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFBLE9BQU8sSUFBSSxPQUFBLEtBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxFQUF0QixDQUFzQixDQUFDLENBQUM7Z0JBQ3RFLENBQUM7Z0JBRUQsaUNBQU0sR0FBTjtvQkFDQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7Z0JBQ25DLENBQUM7Z0JBRUQscUNBQVUsR0FBVixVQUFXLE1BQU07b0JBQ2hCLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsRUFBRSxLQUFLLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQSxDQUFDO3dCQUN4QyxJQUFJLENBQUMsY0FBYyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7d0JBQzNCLE1BQU0sQ0FBQztvQkFDUixDQUFDO29CQUVELElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQzdELENBQUM7Z0JBRUQsbUNBQVEsR0FBUjtvQkFDQyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDO29CQUUxQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsWUFBWSxDQUFDLENBQUM7b0JBQ3pDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO3dCQUNsQixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztvQkFDL0IsQ0FBQztvQkFBQyxJQUFJLENBQUMsQ0FBQzt3QkFDUCxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7b0JBQ2YsQ0FBQztnQkFFRixDQUFDO2dCQWpEQTtvQkFBQyxZQUFLLEVBQUU7O2lFQUFBO2dCQVBWO29CQUFDLGdCQUFTLENBQUM7d0JBQ1YsUUFBUSxFQUFFLFNBQVM7d0JBQ25CLFdBQVcsRUFBRSw0QkFBNEI7d0JBQ3ZDLFNBQVMsRUFBRSxDQUFDLGdDQUFjLEVBQUUsOEJBQWEsQ0FBQztxQkFDNUMsQ0FBQzs7b0NBQUE7Z0JBcURGLHVCQUFDO1lBQUQsQ0FuREEsQUFtREMsSUFBQTtZQW5ERCwrQ0FtREMsQ0FBQSIsImZpbGUiOiJtZW1iZXJzLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dCwgSW5qZWN0b3J9IGZyb20gJ2FuZ3VsYXIyL2NvcmUnO1xuaW1wb3J0IHsgTWVtYmVyc1NlcnZpY2UgfSBmcm9tICcuL21lbWJlcnMuc2VydmljZSc7XG5pbXBvcnQgeyBFbGRlcnNTZXJ2aWNlIH0gZnJvbSAnLi9lbGRlcnMuc2VydmljZSc7XG5pbXBvcnQgeyBNZW1iZXIgfSBmcm9tICcuL01lbWJlcic7XG5pbXBvcnQgeyBSb3V0ZXIsIFJvdXRlUGFyYW1zIH0gZnJvbSAnYW5ndWxhcjIvcm91dGVyJztcblxuXG5AQ29tcG9uZW50KHtcblx0c2VsZWN0b3I6ICdtZW1iZXJzJyxcblx0dGVtcGxhdGVVcmw6ICd0ZW1wbGF0ZXMvbWVtYmVycy50cGwuaHRtbCcsXG4gIFx0cHJvdmlkZXJzOiBbTWVtYmVyc1NlcnZpY2UsIEVsZGVyc1NlcnZpY2VdXG59KVxuXG5leHBvcnQgY2xhc3MgTWVtYmVyc0NvbXBvbmVudHtcbiBcdEBJbnB1dCgpIFxuIFx0bWVtYmVyczogTWVtYmVyW107XG4gXHRwYXJhbXM6IFJvdXRlUGFyYW1zO1xuIFx0ZWxkZXJJZDogc3RyaW5nO1xuIFx0c2VsZWN0ZWRNZW1iZXI6IE1lbWJlcjtcblx0ZWxkZXJOYW1lOiBzdHJpbmc7XG5cblx0Y29uc3RydWN0b3IocHJpdmF0ZSBtZW1iZXJzU2VydmljZTogTWVtYmVyc1NlcnZpY2UsIFxuXHRcdFx0XHRwcml2YXRlIGVsZGVyc1NlcnZpY2U6IEVsZGVyc1NlcnZpY2UsIFxuXHRcdFx0XHRwcml2YXRlIHJvdXRlcjogUm91dGVyLCBcblx0XHRcdFx0aW5qZWN0b3I6IEluamVjdG9yKSB7IFxuXHRcdHRoaXMucGFyYW1zID0gaW5qZWN0b3IuZ2V0KFJvdXRlUGFyYW1zKTtcblx0XHR0aGlzLnNlbGVjdGVkTWVtYmVyPSBuZXcgTWVtYmVyKDAsIFwiXCIsIFwiXCIsIDApO1xuXHR9XG5cdFxuXHRnZXRNZW1iZXJzKGVsZGVySWQpIHtcblx0XHRjb25zb2xlLmxvZygnZ2V0TWVtYmVyczogJyArIGVsZGVySWQpO1xuXHRcdC8vdGhpcy5tZW1iZXJzU2VydmljZS5nZXRNZW1iZXJzKGVsZGVySWQpLnRoZW4obWVtYmVycyA9PiB0aGlzLm1lbWJlcnMgPSBtZW1iZXJzKTtcblx0XHR0aGlzLm1lbWJlcnMgPSB0aGlzLm1lbWJlcnNTZXJ2aWNlLmdldE1lbWJlcnMoZWxkZXJJZCk7XG5cdFx0dGhpcy5lbGRlck5hbWUgPSB0aGlzLmVsZGVyc1NlcnZpY2UuZ2V0RWxkZXIoZWxkZXJJZCkuZmlyc3ROYW1lO1xuXHR9XG5cblx0Z2V0QWxsKCkge1xuXHRcdHRoaXMubWVtYmVyc1NlcnZpY2UuZ2V0QWxsKCkudGhlbihtZW1iZXJzID0+IHRoaXMubWVtYmVycyA9IG1lbWJlcnMpO1xuXHR9XG5cblx0Z29Ib21lKCkge1xuXHRcdHRoaXMucm91dGVyLm5hdmlnYXRlKFsnL0VsZGVycyddKTtcblx0fVxuXG5cdHNob3dEZXRhaWwobWVtYmVyKSB7XG5cdFx0aWYodGhpcy5zZWxlY3RlZE1lbWJlci5pZCA9PT0gbWVtYmVyLmlkKXsvL2lmIHVzZXIgaXMgdHJ5aW5nIHRvIGNvbGxhcHNlXG5cdFx0XHR0aGlzLnNlbGVjdGVkTWVtYmVyLmlkID0gMDtcblx0XHRcdHJldHVybjtcblx0XHR9XG5cblx0XHR0aGlzLnNlbGVjdGVkTWVtYmVyID0gdGhpcy5tZW1iZXJzU2VydmljZS5nZXRNZW1iZXIobWVtYmVyKTtcblx0fVxuXG5cdG5nT25Jbml0KCkge1xuXHRcdHRoaXMuZWxkZXJJZCA9IHRoaXMucGFyYW1zLmdldCgnZWxkZXJJZCcpO1xuXG5cdFx0Y29uc29sZS5sb2codGhpcy5lbGRlcklkICsgXCIgOiBlbGRlcklkXCIpO1xuXHRcdGlmICh0aGlzLmVsZGVySWQpIHtcblx0XHRcdHRoaXMuZ2V0TWVtYmVycyh0aGlzLmVsZGVySWQpO1xuXHRcdH0gZWxzZSB7XG5cdFx0XHR0aGlzLmdldEFsbCgpO1x0XG5cdFx0fVxuXHRcdFxuXHR9XG59Il0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
