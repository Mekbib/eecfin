System.register(['angular2/core', "./elders.component"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, elders_component_1;
    var MyComponentComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (elders_component_1_1) {
                elders_component_1 = elders_component_1_1;
            }],
        execute: function() {
            MyComponentComponent = (function () {
                function MyComponentComponent() {
                    this.name = "my-component";
                }
                MyComponentComponent = __decorate([
                    core_1.Component({
                        selector: 'my-component',
                        template: "\n\t\t<span [style.color]=\"inputElement.value==='yes'? 'red' : ''\">\n\t\tThis is my component's name \n\t\t<span>: \n\t\t<span [class.is-awesome]=\"inputElement.value==='yes'\">{{name}}</span>\n\t\t<input type=\"text\" #inputElement (keyup)=\"0\">\n\t\t<button [disabled]=\"inputElement.value!=='yes'\">isYes?</button>\n\t\t<elders></elders>\n\t",
                        styleUrls: ['src/css/style.css'],
                        directives: [elders_component_1.EldersComponent]
                    }), 
                    __metadata('design:paramtypes', [])
                ], MyComponentComponent);
                return MyComponentComponent;
            }());
            exports_1("MyComponentComponent", MyComponentComponent);
        }
    }
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm15LWNvbXBvbmVudC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUFrQkE7Z0JBQUE7b0JBQ0MsU0FBSSxHQUFHLGNBQWMsQ0FBQTtnQkFDdEIsQ0FBQztnQkFqQkQ7b0JBQUMsZ0JBQVMsQ0FBQzt3QkFDVixRQUFRLEVBQUcsY0FBYzt3QkFDekIsUUFBUSxFQUFHLDZWQVFWO3dCQUNELFNBQVMsRUFBQyxDQUFDLG1CQUFtQixDQUFDO3dCQUMvQixVQUFVLEVBQUUsQ0FBQyxrQ0FBZSxDQUFDO3FCQUM3QixDQUFDOzt3Q0FBQTtnQkFJRiwyQkFBQztZQUFELENBRkEsQUFFQyxJQUFBO1lBRkQsdURBRUMsQ0FBQSIsImZpbGUiOiJteS1jb21wb25lbnQuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnR9IGZyb20gJ2FuZ3VsYXIyL2NvcmUnO1xuaW1wb3J0IHtFbGRlcnNDb21wb25lbnR9IGZyb20gXCIuL2VsZGVycy5jb21wb25lbnRcIjtcblxuQENvbXBvbmVudCh7XG5cdHNlbGVjdG9yIDogJ215LWNvbXBvbmVudCcsXG5cdHRlbXBsYXRlIDogYFxuXHRcdDxzcGFuIFtzdHlsZS5jb2xvcl09XCJpbnB1dEVsZW1lbnQudmFsdWU9PT0neWVzJz8gJ3JlZCcgOiAnJ1wiPlxuXHRcdFRoaXMgaXMgbXkgY29tcG9uZW50J3MgbmFtZSBcblx0XHQ8c3Bhbj46IFxuXHRcdDxzcGFuIFtjbGFzcy5pcy1hd2Vzb21lXT1cImlucHV0RWxlbWVudC52YWx1ZT09PSd5ZXMnXCI+e3tuYW1lfX08L3NwYW4+XG5cdFx0PGlucHV0IHR5cGU9XCJ0ZXh0XCIgI2lucHV0RWxlbWVudCAoa2V5dXApPVwiMFwiPlxuXHRcdDxidXR0b24gW2Rpc2FibGVkXT1cImlucHV0RWxlbWVudC52YWx1ZSE9PSd5ZXMnXCI+aXNZZXM/PC9idXR0b24+XG5cdFx0PGVsZGVycz48L2VsZGVycz5cblx0YCxcblx0c3R5bGVVcmxzOlsnc3JjL2Nzcy9zdHlsZS5jc3MnXSxcblx0ZGlyZWN0aXZlczogW0VsZGVyc0NvbXBvbmVudF1cbn0pXG5cbmV4cG9ydCBjbGFzcyBNeUNvbXBvbmVudENvbXBvbmVudCB7XG5cdG5hbWUgPSBcIm15LWNvbXBvbmVudFwiXG59Il0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
