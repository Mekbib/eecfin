System.register(['angular2/core', './elder-mock'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, elder_mock_1;
    var EldersService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (elder_mock_1_1) {
                elder_mock_1 = elder_mock_1_1;
            }],
        execute: function() {
            EldersService = (function () {
                function EldersService() {
                }
                EldersService.prototype.getElders = function () {
                    return Promise.resolve(elder_mock_1.ELDERS);
                };
                EldersService.prototype.getElder = function (elderId) {
                    console.log(elderId);
                    var elders;
                    elders = elder_mock_1.ELDERS.filter(function (elder) {
                        console.log(elder);
                        return elderId === elder.id;
                    });
                    // this.getElders().then(elders => elders = elders.filter(function(thisElder) {
                    // 	return elderId === thisElder.id;
                    // }));
                    console.log(elders);
                    return elders[0];
                };
                EldersService.prototype.getEldersSlowly = function () {
                    return new Promise(function (resolve) {
                        return setTimeout(function () { return resolve(elder_mock_1.ELDERS); }, 2000);
                    } // 2 seconds
                     // 2 seconds
                    );
                };
                EldersService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [])
                ], EldersService);
                return EldersService;
            }());
            exports_1("EldersService", EldersService);
        }
    }
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImVsZGVycy5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1lBTUE7Z0JBQUE7Z0JBNkJBLENBQUM7Z0JBNUJBLGlDQUFTLEdBQVQ7b0JBQ0MsTUFBTSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsbUJBQU0sQ0FBQyxDQUFDO2dCQUNoQyxDQUFDO2dCQUVELGdDQUFRLEdBQVIsVUFBUyxPQUFPO29CQUNmLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUM7b0JBRXJCLElBQUksTUFBZSxDQUFDO29CQUVwQixNQUFNLEdBQUcsbUJBQU0sQ0FBQyxNQUFNLENBQUMsVUFBUyxLQUFLO3dCQUNwQyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO3dCQUVuQixNQUFNLENBQUMsT0FBTyxLQUFLLEtBQUssQ0FBQyxFQUFFLENBQUM7b0JBQzdCLENBQUMsQ0FBQyxDQUFDO29CQUNILCtFQUErRTtvQkFDL0Usb0NBQW9DO29CQUNwQyxPQUFPO29CQUVQLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7b0JBRXBCLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2xCLENBQUM7Z0JBRUQsdUNBQWUsR0FBZjtvQkFDQyxNQUFNLENBQUMsSUFBSSxPQUFPLENBQVUsVUFBQSxPQUFPO3dCQUNsQyxPQUFBLFVBQVUsQ0FBQyxjQUFJLE9BQUEsT0FBTyxDQUFDLG1CQUFNLENBQUMsRUFBZixDQUFlLEVBQUUsSUFBSSxDQUFDO29CQUFyQyxDQUFxQyxDQUFDLFlBQVk7b0JBQWIsQ0FBQyxZQUFZO3FCQUNsRCxDQUFDO2dCQUNILENBQUM7Z0JBN0JGO29CQUFDLGlCQUFVLEVBQUU7O2lDQUFBO2dCQThCYixvQkFBQztZQUFELENBN0JBLEFBNkJDLElBQUE7WUE3QkQseUNBNkJDLENBQUEiLCJmaWxlIjoiZWxkZXJzLnNlcnZpY2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnYW5ndWxhcjIvY29yZSc7XG5cbmltcG9ydCB7IEVMREVSUyB9IGZyb20gJy4vZWxkZXItbW9jayc7XG5pbXBvcnQgeyBFbGRlciB9IGZyb20gJy4vRWxkZXInO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgRWxkZXJzU2VydmljZSB7XG5cdGdldEVsZGVycygpIHtcblx0XHRyZXR1cm4gUHJvbWlzZS5yZXNvbHZlKEVMREVSUyk7XG5cdH1cblxuXHRnZXRFbGRlcihlbGRlcklkKXtcblx0XHRjb25zb2xlLmxvZyhlbGRlcklkKTtcblxuXHRcdGxldCBlbGRlcnM6IEVsZGVyW107XG5cblx0XHRlbGRlcnMgPSBFTERFUlMuZmlsdGVyKGZ1bmN0aW9uKGVsZGVyKSB7XG5cdFx0XHRjb25zb2xlLmxvZyhlbGRlcik7XG5cblx0XHRcdHJldHVybiBlbGRlcklkID09PSBlbGRlci5pZDtcblx0XHR9KTtcblx0XHQvLyB0aGlzLmdldEVsZGVycygpLnRoZW4oZWxkZXJzID0+IGVsZGVycyA9IGVsZGVycy5maWx0ZXIoZnVuY3Rpb24odGhpc0VsZGVyKSB7XG5cdFx0Ly8gXHRyZXR1cm4gZWxkZXJJZCA9PT0gdGhpc0VsZGVyLmlkO1xuXHRcdC8vIH0pKTtcblxuXHRcdGNvbnNvbGUubG9nKGVsZGVycyk7XG5cblx0XHRyZXR1cm4gZWxkZXJzWzBdO1xuXHR9XG5cblx0Z2V0RWxkZXJzU2xvd2x5KCkge1xuXHRcdHJldHVybiBuZXcgUHJvbWlzZTxFbGRlcltdPihyZXNvbHZlID0+XG5cdFx0XHRzZXRUaW1lb3V0KCgpPT5yZXNvbHZlKEVMREVSUyksIDIwMDApIC8vIDIgc2Vjb25kc1xuXHRcdCk7XG5cdH1cbn0iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
