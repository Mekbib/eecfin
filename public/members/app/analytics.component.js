System.register(['angular2/core'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var AnalyticsComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            AnalyticsComponent = (function () {
                function AnalyticsComponent() {
                }
                AnalyticsComponent = __decorate([
                    core_1.Component({
                        selector: 'analytics',
                        templateUrl: 'templates/analytics.tpl.html'
                    }), 
                    __metadata('design:paramtypes', [])
                ], AnalyticsComponent);
                return AnalyticsComponent;
            }());
            exports_1("AnalyticsComponent", AnalyticsComponent);
        }
    }
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFuYWx5dGljcy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUFPQTtnQkFFQztnQkFDQSxDQUFDO2dCQVJGO29CQUFDLGdCQUFTLENBQUM7d0JBQ1YsUUFBUSxFQUFFLFdBQVc7d0JBQ3JCLFdBQVcsRUFBRSw4QkFBOEI7cUJBQzNDLENBQUM7O3NDQUFBO2dCQU9GLHlCQUFDO1lBQUQsQ0FMQSxBQUtDLElBQUE7WUFMRCxtREFLQyxDQUFBIiwiZmlsZSI6ImFuYWx5dGljcy5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgSW5wdXR9IGZyb20gJ2FuZ3VsYXIyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcblx0c2VsZWN0b3I6ICdhbmFseXRpY3MnLFxuXHR0ZW1wbGF0ZVVybDogJ3RlbXBsYXRlcy9hbmFseXRpY3MudHBsLmh0bWwnXG59KVxuXG5leHBvcnQgY2xhc3MgQW5hbHl0aWNzQ29tcG9uZW50e1xuIFx0XG5cdGNvbnN0cnVjdG9yKCkgeyBcblx0fVxuXHRcbn0iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
