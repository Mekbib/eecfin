System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var ELDERS;
    return {
        setters:[],
        execute: function() {
            exports_1("ELDERS", ELDERS = [
                { id: 1, firstName: "Yetesha", lastName: "Bogale" },
                { id: 2, firstName: "Tamrat", lastName: "Teshome" },
                { id: 3, firstName: "Yavello", lastName: "Nataye" },
                { id: 4, firstName: "Mekbib", lastName: "Tekle" }
            ]);
        }
    }
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImVsZGVyLW1vY2sudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O1FBRWEsTUFBTTs7OztZQUFOLG9CQUFBLE1BQU0sR0FBWTtnQkFDOUIsRUFBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsUUFBUSxFQUFDLFFBQVEsRUFBQztnQkFDaEQsRUFBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFLFNBQVMsRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFDLFNBQVMsRUFBQztnQkFDaEQsRUFBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsUUFBUSxFQUFDLFFBQVEsRUFBQztnQkFDaEQsRUFBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFLFNBQVMsRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFLE9BQU8sRUFBQzthQUMvQyxDQUFBLENBQUMiLCJmaWxlIjoiZWxkZXItbW9jay5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7RWxkZXJ9IGZyb20gXCIuL0VsZGVyXCI7XG5cbmV4cG9ydCBjb25zdCBFTERFUlM6IEVsZGVyW10gPSBbXG5cdHtpZDogMSwgZmlyc3ROYW1lOiBcIllldGVzaGFcIiwgbGFzdE5hbWU6XCJCb2dhbGVcIn0sXG5cdHtpZDogMiwgZmlyc3ROYW1lOiBcIlRhbXJhdFwiLCBsYXN0TmFtZTpcIlRlc2hvbWVcIn0sXG5cdHtpZDogMywgZmlyc3ROYW1lOiBcIllhdmVsbG9cIiwgbGFzdE5hbWU6XCJOYXRheWVcIn0sXG5cdHtpZDogNCwgZmlyc3ROYW1lOiBcIk1la2JpYlwiLCBsYXN0TmFtZTogXCJUZWtsZVwifVxuXTsiXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
