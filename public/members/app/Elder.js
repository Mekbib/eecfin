System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var Elder;
    return {
        setters:[],
        execute: function() {
            Elder = (function () {
                function Elder(id, firstName, lastName) {
                    this.id = id;
                    this.firstName = firstName;
                    this.lastName = lastName;
                }
                return Elder;
            }());
            exports_1("Elder", Elder);
        }
    }
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkVsZGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7WUFBQTtnQkFDQyxlQUFtQixFQUFVLEVBQVMsU0FBaUIsRUFBUyxRQUFnQjtvQkFBN0QsT0FBRSxHQUFGLEVBQUUsQ0FBUTtvQkFBUyxjQUFTLEdBQVQsU0FBUyxDQUFRO29CQUFTLGFBQVEsR0FBUixRQUFRLENBQVE7Z0JBRWhGLENBQUM7Z0JBQ0YsWUFBQztZQUFELENBSkEsQUFJQyxJQUFBO1lBSkQseUJBSUMsQ0FBQSIsImZpbGUiOiJFbGRlci5qcyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBjbGFzcyBFbGRlcntcblx0Y29uc3RydWN0b3IocHVibGljIGlkOiBudW1iZXIsIHB1YmxpYyBmaXJzdE5hbWU6IHN0cmluZywgcHVibGljIGxhc3ROYW1lOiBzdHJpbmcpIHtcblxuXHR9XG59Il0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
