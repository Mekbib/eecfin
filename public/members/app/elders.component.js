System.register(['angular2/core', './elders.service', 'angular2/router'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, elders_service_1, router_1;
    var EldersComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (elders_service_1_1) {
                elders_service_1 = elders_service_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            }],
        execute: function() {
            EldersComponent = (function () {
                function EldersComponent(eldersService, router) {
                    this.eldersService = eldersService;
                    this.router = router;
                }
                EldersComponent.prototype.getElders = function () {
                    var _this = this;
                    this.eldersService.getElders().then(function (elders) { return _this.elders = elders; });
                };
                EldersComponent.prototype.goToEldersMembers = function (elder) {
                    console.log(elder);
                    this.router.navigate(['/Members', { elderId: elder.id }]); //${elder.id}`]);
                };
                EldersComponent.prototype.ngOnInit = function () {
                    this.getElders();
                };
                __decorate([
                    core_1.Input(), 
                    __metadata('design:type', Array)
                ], EldersComponent.prototype, "elders", void 0);
                EldersComponent = __decorate([
                    core_1.Component({
                        selector: 'elders',
                        templateUrl: 'templates/elders.tpl.html',
                        providers: [elders_service_1.EldersService]
                    }), 
                    __metadata('design:paramtypes', [elders_service_1.EldersService, router_1.Router])
                ], EldersComponent);
                return EldersComponent;
            }());
            exports_1("EldersComponent", EldersComponent);
        }
    }
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImVsZGVycy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUFZQTtnQkFLQyx5QkFBb0IsYUFBNEIsRUFBVSxNQUFjO29CQUFwRCxrQkFBYSxHQUFiLGFBQWEsQ0FBZTtvQkFBVSxXQUFNLEdBQU4sTUFBTSxDQUFRO2dCQUN4RSxDQUFDO2dCQUVELG1DQUFTLEdBQVQ7b0JBQUEsaUJBRUM7b0JBREEsSUFBSSxDQUFDLGFBQWEsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQSxNQUFNLElBQUksT0FBQSxLQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sRUFBcEIsQ0FBb0IsQ0FBQyxDQUFDO2dCQUNyRSxDQUFDO2dCQUVELDJDQUFpQixHQUFqQixVQUFrQixLQUFZO29CQUM3QixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNoQixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLFVBQVUsRUFBRyxFQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsRUFBRSxFQUFDLENBQUMsQ0FBQyxDQUFDLENBQUEsaUJBQWlCO2dCQUM5RSxDQUFDO2dCQUVELGtDQUFRLEdBQVI7b0JBQ0MsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO2dCQUNsQixDQUFDO2dCQWxCQztvQkFBQyxZQUFLLEVBQUU7OytEQUFBO2dCQVBYO29CQUFDLGdCQUFTLENBQUM7d0JBQ1YsUUFBUSxFQUFFLFFBQVE7d0JBQ2xCLFdBQVcsRUFBRSwyQkFBMkI7d0JBQ3RDLFNBQVMsRUFBRSxDQUFDLDhCQUFhLENBQUM7cUJBQzVCLENBQUM7O21DQUFBO2dCQXNCRixzQkFBQztZQUFELENBcEJBLEFBb0JDLElBQUE7WUFwQkQsNkNBb0JDLENBQUEiLCJmaWxlIjoiZWxkZXJzLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dH0gZnJvbSAnYW5ndWxhcjIvY29yZSc7XG5pbXBvcnQgeyBFbGRlcnNTZXJ2aWNlIH0gZnJvbSAnLi9lbGRlcnMuc2VydmljZSc7XG5pbXBvcnQgeyBFbGRlciB9IGZyb20gJy4vRWxkZXInO1xuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnYW5ndWxhcjIvcm91dGVyJztcblxuXG5AQ29tcG9uZW50KHtcblx0c2VsZWN0b3I6ICdlbGRlcnMnLFxuXHR0ZW1wbGF0ZVVybDogJ3RlbXBsYXRlcy9lbGRlcnMudHBsLmh0bWwnLFxuICBcdHByb3ZpZGVyczogW0VsZGVyc1NlcnZpY2VdXG59KVxuXG5leHBvcnQgY2xhc3MgRWxkZXJzQ29tcG9uZW50e1xuICBcdEBJbnB1dCgpIFxuXHRlbGRlcnM6IEVsZGVyW107XG5cblxuXHRjb25zdHJ1Y3Rvcihwcml2YXRlIGVsZGVyc1NlcnZpY2U6IEVsZGVyc1NlcnZpY2UsIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIpIHsgXG5cdH1cblx0XG5cdGdldEVsZGVycygpIHtcblx0XHR0aGlzLmVsZGVyc1NlcnZpY2UuZ2V0RWxkZXJzKCkudGhlbihlbGRlcnMgPT4gdGhpcy5lbGRlcnMgPSBlbGRlcnMpO1xuXHR9XG5cblx0Z29Ub0VsZGVyc01lbWJlcnMoZWxkZXI6IEVsZGVyKSB7XG5cdFx0Y29uc29sZS5sb2coZWxkZXIpO1xuICAgIFx0dGhpcy5yb3V0ZXIubmF2aWdhdGUoWycvTWVtYmVycycsICB7ZWxkZXJJZDogZWxkZXIuaWR9XSk7Ly8ke2VsZGVyLmlkfWBdKTtcblx0fVxuXG5cdG5nT25Jbml0KCkge1xuXHRcdHRoaXMuZ2V0RWxkZXJzKCk7XG5cdH1cbn0iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
