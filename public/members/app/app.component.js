System.register(['angular2/core', './elders.component', './members.component', './analytics.component', 'angular2/router'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, elders_component_1, members_component_1, analytics_component_1, router_1;
    var AppComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (elders_component_1_1) {
                elders_component_1 = elders_component_1_1;
            },
            function (members_component_1_1) {
                members_component_1 = members_component_1_1;
            },
            function (analytics_component_1_1) {
                analytics_component_1 = analytics_component_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            }],
        execute: function() {
            AppComponent = (function () {
                function AppComponent(router) {
                    this.router = router;
                }
                AppComponent.prototype.ngOnInit = function () {
                    this.router.navigate(['/Elders']);
                };
                AppComponent = __decorate([
                    core_1.Component({
                        selector: 'my-app',
                        templateUrl: 'templates/app.tpl.html',
                        directives: [router_1.ROUTER_DIRECTIVES],
                        providers: [router_1.ROUTER_PROVIDERS]
                    }),
                    router_1.RouteConfig([
                        { path: '/elders', component: elders_component_1.EldersComponent, name: 'Elders' },
                        { path: '/members/elder/:elderId', component: members_component_1.MembersComponent, name: 'Members' },
                        { path: '/analytics', component: analytics_component_1.AnalyticsComponent, name: 'Analytics' }
                    ]), 
                    __metadata('design:paramtypes', [router_1.Router])
                ], AppComponent);
                return AppComponent;
            }());
            exports_1("AppComponent", AppComponent);
        }
    }
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUFvQkE7Z0JBRUMsc0JBQW9CLE1BQWM7b0JBQWQsV0FBTSxHQUFOLE1BQU0sQ0FBUTtnQkFFbEMsQ0FBQztnQkFFRCwrQkFBUSxHQUFSO29CQUNDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztnQkFDbkMsQ0FBQztnQkFyQkY7b0JBQUMsZ0JBQVMsQ0FBQzt3QkFDUCxRQUFRLEVBQUUsUUFBUTt3QkFDckIsV0FBVyxFQUFFLHdCQUF3Qjt3QkFDbEMsVUFBVSxFQUFFLENBQUMsMEJBQWlCLENBQUM7d0JBQ2hDLFNBQVMsRUFBRSxDQUFDLHlCQUFnQixDQUFDO3FCQUMvQixDQUFDO29CQUVELG9CQUFXLENBQUM7d0JBQ1osRUFBQyxJQUFJLEVBQUUsU0FBUyxFQUFHLFNBQVMsRUFBRSxrQ0FBZSxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUM7d0JBQzlELEVBQUMsSUFBSSxFQUFFLHlCQUF5QixFQUFFLFNBQVMsRUFBRSxvQ0FBZ0IsRUFBRyxJQUFJLEVBQUUsU0FBUyxFQUFDO3dCQUNoRixFQUFDLElBQUksRUFBRSxZQUFZLEVBQUUsU0FBUyxFQUFFLHdDQUFrQixFQUFFLElBQUksRUFBRSxXQUFXLEVBQUM7cUJBQ3RFLENBQUM7O2dDQUFBO2dCQVdGLG1CQUFDO1lBQUQsQ0FUQSxBQVNDLElBQUE7WUFURCx1Q0FTQyxDQUFBIiwiZmlsZSI6ImFwcC5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgT25Jbml0fSBmcm9tICdhbmd1bGFyMi9jb3JlJztcbmltcG9ydCB7IEVsZGVyc0NvbXBvbmVudCB9IGZyb20gJy4vZWxkZXJzLmNvbXBvbmVudCdcbmltcG9ydCB7IE1lbWJlcnNDb21wb25lbnQgfSBmcm9tICcuL21lbWJlcnMuY29tcG9uZW50J1xuaW1wb3J0IHsgQW5hbHl0aWNzQ29tcG9uZW50IH0gZnJvbSAnLi9hbmFseXRpY3MuY29tcG9uZW50J1xuXG5pbXBvcnQgeyBSb3V0ZUNvbmZpZywgUm91dGVyLCBST1VURVJfRElSRUNUSVZFUywgUk9VVEVSX1BST1ZJREVSUyB9IGZyb20gJ2FuZ3VsYXIyL3JvdXRlcic7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnbXktYXBwJyxcblx0dGVtcGxhdGVVcmw6ICd0ZW1wbGF0ZXMvYXBwLnRwbC5odG1sJyxcbiAgICBkaXJlY3RpdmVzOiBbUk9VVEVSX0RJUkVDVElWRVNdLFxuICBcdHByb3ZpZGVyczogW1JPVVRFUl9QUk9WSURFUlNdXG59KVxuXG5AUm91dGVDb25maWcoW1xuXHR7cGF0aDogJy9lbGRlcnMnLCAgY29tcG9uZW50OiBFbGRlcnNDb21wb25lbnQsIG5hbWU6ICdFbGRlcnMnfSxcblx0e3BhdGg6ICcvbWVtYmVycy9lbGRlci86ZWxkZXJJZCcsIGNvbXBvbmVudDogTWVtYmVyc0NvbXBvbmVudCAsIG5hbWU6ICdNZW1iZXJzJ30sXG5cdHtwYXRoOiAnL2FuYWx5dGljcycsIGNvbXBvbmVudDogQW5hbHl0aWNzQ29tcG9uZW50LCBuYW1lOiAnQW5hbHl0aWNzJ31cbl0pXG5cbmV4cG9ydCBjbGFzcyBBcHBDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXR7XG5cblx0Y29uc3RydWN0b3IocHJpdmF0ZSByb3V0ZXI6IFJvdXRlcikgeyBcblxuXHR9XG5cblx0bmdPbkluaXQoKSB7XG5cdFx0dGhpcy5yb3V0ZXIubmF2aWdhdGUoWycvRWxkZXJzJ10pO1xuXHR9XG59Il0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
