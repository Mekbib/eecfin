System.register(['angular2/core', './member-mock'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, member_mock_1;
    var MembersService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (member_mock_1_1) {
                member_mock_1 = member_mock_1_1;
            }],
        execute: function() {
            MembersService = (function () {
                function MembersService() {
                }
                MembersService.prototype.getAll = function () {
                    return Promise.resolve(member_mock_1.MEMBERS);
                };
                MembersService.prototype.getMember = function (member) {
                    return {
                        id: member.id,
                        firstName: member.firstName,
                        lastName: member.lastName,
                        elderId: member.elderId,
                        elder: "Elder's Name",
                        phoneNumber: "555 123 45 678",
                        email: "email@domain.com",
                        attendance: [0, 1, 1, 0, 1, 1, 1, 1, 1, 0]
                    };
                };
                MembersService.prototype.getMembers = function (elderId) {
                    console.log("service: getMembers.." + elderId);
                    // return new Promise<Member[]>(resolve =>
                    // 	setTimeout(()=>resolve( MEMBERS), 2000) // 2 seconds
                    // );
                    var members;
                    members = member_mock_1.MEMBERS.filter(function (member) {
                        return elderId === member.elderId;
                    });
                    return members;
                };
                MembersService.prototype.getMembersSlowly = function () {
                    return new Promise(function (resolve) {
                        return setTimeout(function () { return resolve(member_mock_1.MEMBERS); }, 2000);
                    } // 2 seconds
                     // 2 seconds
                    );
                };
                MembersService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [])
                ], MembersService);
                return MembersService;
            }());
            exports_1("MembersService", MembersService);
        }
    }
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1lbWJlcnMuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztZQU1BO2dCQUFBO2dCQXNDQSxDQUFDO2dCQXBDQSwrQkFBTSxHQUFOO29CQUNDLE1BQU0sQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLHFCQUFPLENBQUMsQ0FBQztnQkFDakMsQ0FBQztnQkFFRCxrQ0FBUyxHQUFULFVBQVcsTUFBTTtvQkFDaEIsTUFBTSxDQUFDO3dCQUNOLEVBQUUsRUFBRyxNQUFNLENBQUMsRUFBRTt3QkFDZCxTQUFTLEVBQUUsTUFBTSxDQUFDLFNBQVM7d0JBQzNCLFFBQVEsRUFBRSxNQUFNLENBQUMsUUFBUTt3QkFDekIsT0FBTyxFQUFFLE1BQU0sQ0FBQyxPQUFPO3dCQUN2QixLQUFLLEVBQUUsY0FBYzt3QkFDckIsV0FBVyxFQUFFLGdCQUFnQjt3QkFDN0IsS0FBSyxFQUFFLGtCQUFrQjt3QkFDekIsVUFBVSxFQUFFLENBQUMsQ0FBQyxFQUFDLENBQUMsRUFBQyxDQUFDLEVBQUMsQ0FBQyxFQUFDLENBQUMsRUFBQyxDQUFDLEVBQUMsQ0FBQyxFQUFDLENBQUMsRUFBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDO3FCQUNqQyxDQUFBO2dCQUNGLENBQUM7Z0JBRUQsbUNBQVUsR0FBVixVQUFZLE9BQU87b0JBQ2xCLE9BQU8sQ0FBQyxHQUFHLENBQUMsdUJBQXVCLEdBQUcsT0FBTyxDQUFDLENBQUM7b0JBQy9DLDBDQUEwQztvQkFDMUMsd0RBQXdEO29CQUN4RCxLQUFLO29CQUNMLElBQUksT0FBaUIsQ0FBQztvQkFFdEIsT0FBTyxHQUFHLHFCQUFPLENBQUMsTUFBTSxDQUFDLFVBQVUsTUFBTTt3QkFDeEMsTUFBTSxDQUFDLE9BQU8sS0FBSyxNQUFNLENBQUMsT0FBTyxDQUFDO29CQUNuQyxDQUFDLENBQUMsQ0FBQztvQkFFSCxNQUFNLENBQUMsT0FBTyxDQUFDO2dCQUNoQixDQUFDO2dCQUVELHlDQUFnQixHQUFoQjtvQkFDQyxNQUFNLENBQUMsSUFBSSxPQUFPLENBQVcsVUFBQSxPQUFPO3dCQUNuQyxPQUFBLFVBQVUsQ0FBQyxjQUFJLE9BQUEsT0FBTyxDQUFDLHFCQUFPLENBQUMsRUFBaEIsQ0FBZ0IsRUFBRSxJQUFJLENBQUM7b0JBQXRDLENBQXNDLENBQUMsWUFBWTtvQkFBYixDQUFDLFlBQVk7cUJBQ25ELENBQUM7Z0JBQ0gsQ0FBQztnQkF0Q0Y7b0JBQUMsaUJBQVUsRUFBRTs7a0NBQUE7Z0JBdUNiLHFCQUFDO1lBQUQsQ0F0Q0EsQUFzQ0MsSUFBQTtZQXRDRCwyQ0FzQ0MsQ0FBQSIsImZpbGUiOiJtZW1iZXJzLnNlcnZpY2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnYW5ndWxhcjIvY29yZSc7XG5cbmltcG9ydCB7IE1FTUJFUlMgfSBmcm9tICcuL21lbWJlci1tb2NrJztcbmltcG9ydCB7IE1lbWJlciB9IGZyb20gJy4vTWVtYmVyJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIE1lbWJlcnNTZXJ2aWNlIHtcblxuXHRnZXRBbGwoKSB7XG5cdFx0cmV0dXJuIFByb21pc2UucmVzb2x2ZShNRU1CRVJTKTtcblx0fVxuXG5cdGdldE1lbWJlciAobWVtYmVyKSB7XG5cdFx0cmV0dXJuIHtcblx0XHRcdGlkIDogbWVtYmVyLmlkLFxuXHRcdFx0Zmlyc3ROYW1lOiBtZW1iZXIuZmlyc3ROYW1lLFxuXHRcdFx0bGFzdE5hbWU6IG1lbWJlci5sYXN0TmFtZSxcblx0XHRcdGVsZGVySWQ6IG1lbWJlci5lbGRlcklkLFxuXHRcdFx0ZWxkZXI6IFwiRWxkZXIncyBOYW1lXCIsXG5cdFx0XHRwaG9uZU51bWJlcjogXCI1NTUgMTIzIDQ1IDY3OFwiLFxuXHRcdFx0ZW1haWw6IFwiZW1haWxAZG9tYWluLmNvbVwiLFxuXHRcdFx0YXR0ZW5kYW5jZTogWzAsMSwxLDAsMSwxLDEsMSwxLDBdXG5cdFx0fVxuXHR9XG5cblx0Z2V0TWVtYmVycyAoZWxkZXJJZCkge1xuXHRcdGNvbnNvbGUubG9nKFwic2VydmljZTogZ2V0TWVtYmVycy4uXCIgKyBlbGRlcklkKTtcblx0XHQvLyByZXR1cm4gbmV3IFByb21pc2U8TWVtYmVyW10+KHJlc29sdmUgPT5cblx0XHQvLyBcdHNldFRpbWVvdXQoKCk9PnJlc29sdmUoIE1FTUJFUlMpLCAyMDAwKSAvLyAyIHNlY29uZHNcblx0XHQvLyApO1xuXHRcdGxldCBtZW1iZXJzOiBNZW1iZXJbXTtcblxuXHRcdG1lbWJlcnMgPSBNRU1CRVJTLmZpbHRlcihmdW5jdGlvbiAobWVtYmVyKSB7XG5cdFx0XHRyZXR1cm4gZWxkZXJJZCA9PT0gbWVtYmVyLmVsZGVySWQ7XG5cdFx0fSk7XG5cblx0XHRyZXR1cm4gbWVtYmVycztcblx0fVxuXG5cdGdldE1lbWJlcnNTbG93bHkoKSB7XG5cdFx0cmV0dXJuIG5ldyBQcm9taXNlPE1lbWJlcltdPihyZXNvbHZlID0+XG5cdFx0XHRzZXRUaW1lb3V0KCgpPT5yZXNvbHZlKE1FTUJFUlMpLCAyMDAwKSAvLyAyIHNlY29uZHNcblx0XHQpO1xuXHR9XG59Il0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
