import {Component, OnInit} from 'angular2/core';
import { EldersComponent } from './elders.component'
import { MembersComponent } from './members.component'
import { AnalyticsComponent } from './analytics.component'

import { RouteConfig, Router, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';

@Component({
    selector: 'my-app',
	templateUrl: 'templates/app.tpl.html',
    directives: [ROUTER_DIRECTIVES],
  	providers: [ROUTER_PROVIDERS]
})

@RouteConfig([
	{path: '/elders',  component: EldersComponent, name: 'Elders'},
	{path: '/members/elder/:elderId', component: MembersComponent , name: 'Members'},
	{path: '/analytics', component: AnalyticsComponent, name: 'Analytics'}
])

export class AppComponent implements OnInit{

	constructor(private router: Router) { 

	}

	ngOnInit() {
		this.router.navigate(['/Elders']);
	}
}