import {Elder} from "./Elder";

export const ELDERS: Elder[] = [
	{id: 1, firstName: "Yetesha", lastName:"Bogale"},
	{id: 2, firstName: "Tamrat", lastName:"Teshome"},
	{id: 3, firstName: "Yavello", lastName:"Nataye"},
	{id: 4, firstName: "Mekbib", lastName: "Tekle"}
];