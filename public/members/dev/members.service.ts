import { Injectable } from 'angular2/core';

import { MEMBERS } from './member-mock';
import { Member } from './Member';

@Injectable()
export class MembersService {

	getAll() {
		return Promise.resolve(MEMBERS);
	}

	getMember (member) {
		return {
			id : member.id,
			firstName: member.firstName,
			lastName: member.lastName,
			elderId: member.elderId,
			elder: "Elder's Name",
			phoneNumber: "555 123 45 678",
			email: "email@domain.com",
			attendance: [0,1,1,0,1,1,1,1,1,0]
		}
	}

	getMembers (elderId) {
		console.log("service: getMembers.." + elderId);
		// return new Promise<Member[]>(resolve =>
		// 	setTimeout(()=>resolve( MEMBERS), 2000) // 2 seconds
		// );
		let members: Member[];

		members = MEMBERS.filter(function (member) {
			return elderId === member.elderId;
		});

		return members;
	}

	getMembersSlowly() {
		return new Promise<Member[]>(resolve =>
			setTimeout(()=>resolve(MEMBERS), 2000) // 2 seconds
		);
	}
}