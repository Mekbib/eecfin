import {Component, Input} from 'angular2/core';
import { EldersService } from './elders.service';
import { Elder } from './Elder';
import { Router } from 'angular2/router';


@Component({
	selector: 'elders',
	templateUrl: 'templates/elders.tpl.html',
  	providers: [EldersService]
})

export class EldersComponent{
  	@Input() 
	elders: Elder[];


	constructor(private eldersService: EldersService, private router: Router) { 
	}
	
	getElders() {
		this.eldersService.getElders().then(elders => this.elders = elders);
	}

	goToEldersMembers(elder: Elder) {
		console.log(elder);
    	this.router.navigate(['/Members',  {elderId: elder.id}]);//${elder.id}`]);
	}

	ngOnInit() {
		this.getElders();
	}
}