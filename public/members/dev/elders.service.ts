import { Injectable } from 'angular2/core';

import { ELDERS } from './elder-mock';
import { Elder } from './Elder';

@Injectable()
export class EldersService {
	getElders() {
		return Promise.resolve(ELDERS);
	}

	getElder(elderId){
		console.log(elderId);

		let elders: Elder[];

		elders = ELDERS.filter(function(elder) {
			console.log(elder);

			return elderId === elder.id;
		});
		// this.getElders().then(elders => elders = elders.filter(function(thisElder) {
		// 	return elderId === thisElder.id;
		// }));

		console.log(elders);

		return elders[0];
	}

	getEldersSlowly() {
		return new Promise<Elder[]>(resolve =>
			setTimeout(()=>resolve(ELDERS), 2000) // 2 seconds
		);
	}
}