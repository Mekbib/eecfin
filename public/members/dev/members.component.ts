import {Component, Input, Injector} from 'angular2/core';
import { MembersService } from './members.service';
import { EldersService } from './elders.service';
import { Member } from './Member';
import { Router, RouteParams } from 'angular2/router';


@Component({
	selector: 'members',
	templateUrl: 'templates/members.tpl.html',
  	providers: [MembersService, EldersService]
})

export class MembersComponent{
 	@Input() 
 	members: Member[];
 	params: RouteParams;
 	elderId: string;
 	selectedMember: Member;
	elderName: string;

	constructor(private membersService: MembersService, 
				private eldersService: EldersService, 
				private router: Router, 
				injector: Injector) { 
		this.params = injector.get(RouteParams);
		this.selectedMember= new Member(0, "", "", 0);
	}
	
	getMembers(elderId) {
		console.log('getMembers: ' + elderId);
		//this.membersService.getMembers(elderId).then(members => this.members = members);
		this.members = this.membersService.getMembers(elderId);
		this.elderName = this.eldersService.getElder(elderId).firstName;
	}

	getAll() {
		this.membersService.getAll().then(members => this.members = members);
	}

	goHome() {
		this.router.navigate(['/Elders']);
	}

	showDetail(member) {
		if(this.selectedMember.id === member.id){//if user is trying to collapse
			this.selectedMember.id = 0;
			return;
		}

		this.selectedMember = this.membersService.getMember(member);
	}

	ngOnInit() {
		this.elderId = this.params.get('elderId');

		console.log(this.elderId + " : elderId");
		if (this.elderId) {
			this.getMembers(this.elderId);
		} else {
			this.getAll();	
		}
		
	}
}